<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 *
 * @TODO Replace classes with specific attributes array.
 */
?>
<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <header class="<?php print $type; ?>__header">
    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2 class="<?php print $type; ?>__title"><a
          href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php if ($page): ?>
      <h1 class="<?php print $type; ?>__title"><?php print $title; ?></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
      <footer class="submitted">
        <?php print $user_picture; ?>
        <p class="submitted__description"><?php print $submitted; ?></p>
      </footer>
    <?php endif; ?>
  </header>
  <div class="<?php print $type ?>__content">
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
    ?>
  </div>
  <footer class="<?php print $type ?>__footer">
    <?php print render($content['links']); ?>
    <?php print render($content['comments']); ?>
  </footer>
</article>
