module.exports = function(grunt) {

    // Init config.
    grunt.initConfig({

        // Register package.
        pkg: grunt.file.readJSON('package.json'),

        // Watch.
        watch: {
            options: {
                livereload: true
            },
            sass: {
                files: ['assets/sass/**/*.scss'],
                tasks: ['sass_globbing', 'sass:dist']
            },
            css: {
                files: ['assets/css/**/*.css']
            }
        },

        // Globbing.
        sass_globbing: {
            import_maps: {
                files: {
                    'assets/sass/imports/_components.scss': 'assets/sass/components/**/*.scss',
                    'assets/sass/imports/_mixins.scss': 'assets/sass/abstractions/mixins/**/*.scss',
                    'assets/sass/imports/_placeholders.scss': 'assets/sass/abstractions/placeholders/**/*.scss',
                    'assets/sass/imports/_base.scss': 'assets/sass/base/**/*.scss'
                }
            }
        },

        // Sass.
        sass: {
            options: {
                includePaths: [
                    'libraries/susy/sass',
                    'libraries/sassy-maps/sass',
                    'libraries/compass-mixins/lib',
                    'libraries/sass-toolkit/stylesheets',
                    'libraries/normalize-scss',
                    'libraries/compass-breakpoint/stylesheets',
                    'libraries/type-heading/stylesheets'
                ]
            },
            dist: {
                sourceMap: true,
                outputStyle: 'extended',
                files: {
                    'assets/css/harris-partners.styles.css': 'assets/sass/harris-partners.styles.scss',
                    'assets/css/harris-partners.hacks.css': 'assets/sass/harris-partners.hacks.scss',
                    'assets/css/harris-partners.harris-partners.normalize.css': 'assets/sass/harris-partners.harris-partners.normalize.scss',
                    'assets/css/harris-partners.no-query.css': 'assets/sass/harris-partners.no-query.scss',
                    'assets/css/layouts/harris_homepage/harris-homepage.layout.css': 'assets/sass/layouts/harris_homepage/harris-homepage.layout.scss',
                    'assets/css/layouts/harris_partners/harris-partners.layout.css': 'assets/sass/layouts/harris_partners/harris-partners.layout.scss'
                }
            }
        },

        // Notify.
        notify: {
            css: {
                options: {
                    title: 'CSS:',
                    message: 'CSS has changed.'
                }
            }
        }

    });

    // Load modules.
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-sass-globbing');

    // Register tasks.
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('glob', ['sass_globbing']);

};