<?php
/**
 * @file Contains all internal functions used by Kurama.
 *
 */

/**
 * @Implements hook_preprocess_node()
 */
function kurama_preprocess_node(&$variables){

  $node = $variables['node'];
  $view_mode = $variables['view_mode'];

  // Add article ARIA role.
  $variables['attributes_array']['role'] = 'article';

  // Reset the array to use Drupal agnostic BEM selectors
  $variables['classes_array'] = array(

    $node->type,
    $node->type . '--' . $view_mode,
  );
  // Set the promoted BEM modifier
  if($variables['promote']){
    $variables['classes_array'][] = $node->type . '--promoted';
  }
}

/**
 * Implements hook_preprocess_field()
 */
function kurama_preprocess_field(&$variables){

  $field_name = preg_replace('/^field_/', '', $variables['element']['#field_name']);
  $variables['field_name'] = $field_name;
  $variables['classes_array'] = array(
    $field_name
  );
}

function kurama_menu_link(array $variables) {

  $element = $variables['element'];
  $element['#attributes']['class'] = array(
    'menu__item'
  );
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements hook_preprocess_block().
 */
function kurama_preprocess_block(&$variables) {

  $delta = $variables['block']->delta;

  $block_name = 'block-' . $delta;

  $variables['attributes_array']['class'] = array(
    $block_name,
    'block'
  );
  $variables['title_attributes_array']['class'] = array(
    $block_name . '__title',
    'block__title'
  );
  $variables['content_attributes_array']['class'] = array(
    $block_name . '__content',
    'block__content'
  );

  // Add template suggestions to appropriate blocks.
  switch ($variables['block']->module) {
    case 'system':
      switch ($variables['block']->delta) {
        case 'help':
        case 'powered-by':
          break;

        case 'main':
          // Use a template with no wrapper for the page's main content.
          $variables['theme_hook_suggestions'][] = 'block__minimal';
          break;

        default:
          // Any other "system" block is a menu block and should use
          // block--nav.tpl.php
          $variables['theme_hook_suggestions'][] = 'block__nav';
          break;
      }
      break;

    case 'menu':
    case 'menu_block':
      // Use block--nav.tpl.php template.
      $variables['theme_hook_suggestions'][] = 'block__nav';
      break;
  }

  // Add Aria Roles via attributes.
  switch ($variables['block']->module) {
    case 'system':
      switch ($variables['block']->delta) {
        case 'main':
          // Note: the "main" role goes in the page.tpl, not here.
          break;

        case 'help':
        case 'powered-by':
          $variables['attributes_array']['role'] = 'complementary';
          break;

        default:
          // Any other "system" block is a menu block.
          $variables['attributes_array']['role'] = 'navigation';
          break;
      }
      break;

    case 'menu':
    case 'menu_block':
    case 'blog':
    case 'book':
    case 'comment':
    case 'forum':
    case 'shortcut':
    case 'statistics':
      $variables['attributes_array']['role'] = 'navigation';
      break;

    case 'search':
      $variables['attributes_array']['role'] = 'search';
      break;

    case 'help':
    case 'aggregator':
    case 'locale':
    case 'poll':
    case 'profile':
      $variables['attributes_array']['role'] = 'complementary';
      break;

    case 'node':
      switch ($variables['block']->delta) {
        case 'syndicate':
          $variables['attributes_array']['role'] = 'complementary';
          break;

        case 'recent':
          $variables['attributes_array']['role'] = 'navigation';
          break;
      }
      break;

    case 'user':
      switch ($variables['block']->delta) {
        case 'login':
          $variables['attributes_array']['role'] = 'form';
          break;

        case 'new':
        case 'online':
          $variables['attributes_array']['role'] = 'complementary';
          break;
      }
      break;
  }
}

/**
 * @file
 * Contains a pre-process hook for 'region'.
 */

/**
 * Implements hook_preprocess_region().
 */
function kurama_preprocess_region(&$variables) {
  // Sidebar regions common template suggestion for all sidebars.
  if (strpos($variables['region'], 'sidebar') === 0) {
    // Allow a region-specific template to override region--sidebar.
    array_unshift($variables['theme_hook_suggestions'], 'region__sidebar');
  }
  // Use a template with no wrapper for the content region.
  elseif ($variables['region'] == 'content') {
    // Allow a region-specific template to override region--minimal.
    array_unshift($variables['theme_hook_suggestions'], 'region__minimal');
  }

  // Use BEM syntax for the modifier class.
  $css_region_name = drupal_clean_css_identifier($variables['region']);
  $variables['attributes_array']['class'] = preg_replace('/^region-' . $css_region_name . '$/', 'l-region--' . $css_region_name, $variables['classes_array']);
  // Use the prefix "l-" on region class names as described in SMACSS.
  $variables['attributes_array']['class'] = preg_replace('/^region$/', 'l-region', $variables['attributes_array']['class']);
}
